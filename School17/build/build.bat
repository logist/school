:::::::::::::::::::::::
:: BUILD BATCH FILE  ::
:::::::::::::::::::::::
::
:: PATH TO CACHE DIRECTORY AND EXECUTABLES 
@echo off
IF NOT DEFINED CACHEDIR SET CACHEDIR=C:\InterSystems\Cache
IF NOT DEFINED CACHEBIN SET CACHEBIN=%CACHEDIR%\bin\cache
IF NOT DEFINED CACHEINSTANCE SET CACHEINSTANCE=CACHE

:: If called from Jenkins, adjust build variables
SET SRCDIR=%CD%\..
SET DBDIR=%SRCDIR%/../db
IF DEFINED WORKSPACE SET SRCDIR=%WORKSPACE%
IF DEFINED WORKSPACE SET DBDIR=%WORKSPACE%/db
SET NAMESPACE=FARMSHARE
IF DEFINED JOB_NAME SET NAMESPACE=%JOB_NAME%

:: PREPARE OUTPUT FILE
set OUTFILE=%SRCDIR%\outFile
del "%OUTFILE%"


:: NOW, PREPARE TO CALL CACHE
::
:: FIRST, LOAD BUILD CLASS TO USER NAMESPACE
echo set sc=$SYSTEM.OBJ.Load("%SRCDIR%\cls\Util\Build.xml","ck") >inFile

:: IF UNSUCCESSFULL, DISPLAY ERROR
echo if sc'=1 do $SYSTEM.OBJ.DisplayError(sc) >>inFile

:: NOW, PERFORM BUILD
IF DEFINED WORKSPACE goto DISMOUNT
goto NODISMOUNT

:DISMOUNT
echo if sc=1 set sc=##class(Util.Build).Build("%NAMESPACE%","%SRCDIR%",1,"%DBDIR%") >>inFile
goto POSTDISMOUNT

:NODISMOUNT
echo if sc=1 set sc=##class(Util.Build).Build("%NAMESPACE%","%SRCDIR%",0,"%DBDIR%") >>inFile

:POSTDISMOUNT

:: IF UNSUCCESSFULL, DISPLAY ERROR
echo if sc'=1 do $SYSTEM.OBJ.DisplayError(sc) >>inFile

:: IF UNSUCCESSFULL, CREATE OUTPUT FLAG FILE
echo if sc'=1 set fileName="%OUTFILE%" o fileName:("NWS") u fileName do $SYSTEM.OBJ.DisplayError(sc) c fileName >>inFile

:: THAT'S IT
echo halt >>inFile

:: CALL CACHE

%CACHEBIN% -s %CACHEDIR%\mgr -U USER <inFile

DEL inFile

echo Build completed. Press enter to exit.

:: PAUSE
pause > nul

:: TEST IF THERE WAS AN ERROR
IF EXIST "%OUTFILE%" EXIT 1
